package main;

//The user can only allocate memory once
public class Memory {
	
	public int[] memory;
	
	public Memory() {
		memory = new int[0];
	}
	
	public int access(int index) {
		return memory[index];
	}
	
	public void set(int index, int value) {
		memory[index] = value;
	}
	
	public void alloc(int numints) {
		if(memory.length == 0)
			memory = new int[numints];
	}
	
}
