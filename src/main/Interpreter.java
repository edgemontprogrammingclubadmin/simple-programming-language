package main;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Interpreter {
	
	public static Scanner in;
	public static String[] program;
	public static Memory memory;
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(args[0]));
		
		int numLines = countLines(args[0]);
		
		program = new String[numLines];
		
		String line;
		int currentLine = 0;
		while((line = br.readLine()) != null) {
			program[currentLine++] = line;
		}
		
		br.close();
		
		memory = new Memory();
		in = new Scanner(System.in);
		interpret(0);
		in.close();
	}
	
	public static void interpret(int line) {
		if(line == program.length) {
			printSuccessMessage();
			return;
		}
		
		try {
		StringTokenizer tokenizer = new StringTokenizer(program[line]);
		
		String operation = tokenizer.nextToken();
		
		switch(operation) {
		case "comment":
		{
			break;
		}
		case "alloc":
		{
			int numInts = Integer.parseInt(tokenizer.nextToken());
			memory.alloc(numInts);
			break;
		}
		case "set":
		{
			int firstPointer = parsePointer(tokenizer.nextToken());
			int value = parseValue(tokenizer.nextToken());
			memory.set(firstPointer, value);
			break;
		}
		case "read":
		{
			int pointer = parsePointer(tokenizer.nextToken());
			memory.set(pointer, in.nextInt());
			break;
		}
		case "add":
		{
			int pointer = parsePointer(tokenizer.nextToken());
			int value = parseValue(tokenizer.nextToken());
			memory.set(pointer, memory.access(pointer) + value);
			break;
		}
		case "print":
		{
			int value = parseValue(tokenizer.nextToken());
			System.out.println(value);
			break;
		}
		case "printchar":
		{
			int value = parseValue(tokenizer.nextToken());
			System.out.print((char)(value - 1 + 'A'));
			break;
		}
		case "jump":
		{
			int nextLine = parseValue(tokenizer.nextToken()) - 1;
			interpret(nextLine);
			return;
		}
		case "jumpif":
		{
			int nextLine = parseValue(tokenizer.nextToken()) - 1;
			int value = parseValue(tokenizer.nextToken());
			if(value != 0) {
				interpret(nextLine);
				return;
			}
		}
		}
		
		interpret(line + 1);
		}catch(Exception e) {
			System.out.println("Fatal error on line " + (line + 1));
			e.printStackTrace();
		}
	}
	
	public static void printSuccessMessage() {
		System.out.println("Success!");
	}
	
	public static int parsePointer(String pointer) {
		int countDeref = 0;
		for(int i = 0; i < pointer.length(); i++) {
			if(pointer.charAt(i) == '*')
				countDeref++;
			else
				break;
		}
		
		String afterDerefs = pointer.substring(countDeref, pointer.length());
		int currentPointer = Integer.parseInt(afterDerefs);
		
		while(countDeref > 0) {
			currentPointer = memory.access(currentPointer);
			countDeref--;
		}
		
		return currentPointer;
	}
	
	public static int parseValue(String value) {
		boolean negative = value.startsWith("-");
		return (negative ? -1 : 1) * parsePointer(value.substring(negative ? 1 : 0, value.length()));
	}
	
	public static int countLines(String path) throws Exception {
		int lines = 0;
		
		BufferedReader br = new BufferedReader(new FileReader(path));
		
		while(br.readLine() != null)
			lines++;
		
		br.close();
		
		
		return lines;
	}
	
}
