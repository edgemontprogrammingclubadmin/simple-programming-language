package main;

import java.util.Scanner;

//A helper tool to help you generate the character codes for various input strings
public class Helper {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String line = sc.nextLine();
		for(char c: line.toCharArray()) {
			System.out.print((int)(c - 'A' + 1) + " ");
		}
		sc.close();
	}
	
}
