*pointer - reference/dereference a pointer (you can negate the returned value by inserting a negative sign before the first "*" character)
<value> a decimal numerical value. Note that negative signs are valid
<pointer> a decimal numerical value that represents the location of an integer in the memory. indices start at 0

Statements
comment <comment> - a comment ignored by the compiler
alloc <num 32-bit integers> - a one-time call (usually at the beginning of a program) that allocates a specific number of integers for the program to use. note that all values are initially set to 0
set <pointer> <value> - sets the value stored at the pointer to the value
add <pointer> <value> - adds the value to the value stored at the pointer
read <pointer> - reads the next integer inputted by the user through the console and stores it in the pointer
print <value> - prints the specified value
printchar <value> - prints the specified value as a character using the ASCII character codes where 1="A"
jump <line> - jumps to the given line - note that the first line number is 1
jumpif <line> <value> - jumps to the given line if the value is not 0

not implemented yet:
mult <pointer> <value> - multiplies the value stored at the pointer with the given value. note that references are valid
